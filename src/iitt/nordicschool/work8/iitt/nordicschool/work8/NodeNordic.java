package iitt.nordicschool.work8;



public class NodeNordic {
    public int value;
    private NodeNordic next;
    private NodeNordic previous;

    public NodeNordic(int value /* NodeNordic previous, NodeNordic next*/) {

         this.value = value;
//        this.previous = previous;
//        this.next = next;
    }

    public int getValue() {
         return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public NodeNordic getNext() {
         return next;
    }

    public void setNext(NodeNordic next) {
        this.next = next;
    }

    public NodeNordic getPrevious() {
        return previous;
    }

    public void setPrevious() {
        this.previous = previous;
    }



}
