package iitt.nordicschool.work8;

public class NordLinkenList {
    private NodeNordic hub = null;
    int size;


    void addToEnd(int value) {
        NodeNordic tmp = new NodeNordic(value);
        tmp.setValue(value);
        tmp.getPrevious();
        System.out.println(value);
        size++;
    }

    public int size() {
        return size;
    }


    public int getByIndex(int index) {


        return findByPosition(index).getValue();
    }

    private NodeNordic findByPosition(int index) {
        NodeNordic current = hub;
        for (int i = 0; i <= index; i++) {
            current = current.getNext();
        }

        return current.getNext();
    }

    public int add(int value, int index) {
        NodeNordic tmpp = new NodeNordic(value);
        tmpp.setValue(value);
        tmpp.setPrevious();
        hub.setPrevious();
        if (index == 0) {
            if (hub == null) {
                tmpp.setNext(null);
            } else {
                tmpp.setNext(hub);
            }

            hub = tmpp;
        } else {
            NodeNordic previous = getByIndex(index - 1);
            tmpp.setNext(previous.getNext());
            previous.setNext(tmpp);

            tmpp.setPrevious();
            if(previous.getNext()!=null){
                previous.getNext().setPrevious();
            }

        }
        return value;
    }
}


